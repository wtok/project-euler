import std.stdio;
import std.math;

int main ()
{
  enum n = 600851475143;
  int answer = eul03 (n);
  
  writeln (answer);
  return 0;
}

/* Determine if integer n is prime by trial division */
bool is_prime (int n)
{
  if (n < 3)
    return 0;

  double s = sqrt (n);
  
  foreach (d; 2 .. ceil (s))
    if (n % d == 0)
      return false;

  return true;
} 

int eul03 (long n)
{
  int ans;
  double s = sqrt (n);
  
  foreach (d; 1 .. s)
    if (remainder (n, 2) == 0 && is_prime (d))
      ans = d;

  return ans;
}

