;; Project Euler, Problem 04:
;;  A palindromic number reads the same both ways.
;;  The largest palindrome made from the product of two 2-digit numbers
;;  is 9009 = 91 × 99. Find the largest palindrome made from the product
;;  of two 3-digit numbers.

;; Determine length of an integer n by dividing n by 10^p for p=0->inf.
;; Return p when result is < 1
(define (integer-length n)
  (let loop ((p 0))
    (cond ((< (/ n (expt 10 p)) 1) p)
          (else (loop (+ p 1))))))

;; Split an integer i into a list containing its digits, l, by dividing
;; i by 10 and consing the remainder onto l. Repeat until i/10 is < 1,
;; then return l. Because cons prepends to a list, l is backwards,
;; necessitating the integer->list function below.
(define (reverse-digits i)
  (let ((l '()))
    (if (< (/ i 10) 1) (cons i l)
        (cons (remainder i 10)
              (reverse-digits (quotient i 10))))))

(define (integer->list i)
  (reverse (reverse-digits i)))

(define (palindrome? l)
  (equal? l (reverse l)))

(define (integer-palindrome? i)
  (palindrome? (integer->list i)))

;; Fairly shamelessly adapted from my Julia eul04 soln.
;; Return largest integer with d digits.
(define (get-max d) 
  (- (expt 10 d) 1))

;; Return smallest integer with d digits.
(define (get-min d)
  (expt 10 (- d 1)))


(define (product-palindrome? l)
  (palindrome? (apply * l)))

;; solution should go here...

