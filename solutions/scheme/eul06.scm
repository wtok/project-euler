;; wtok 2015-09-27
;; find the difference between the sum of squares and square of sum of
;; every integer below 100.


;; produce a list of integers from 1 to maxn
(define (range maxn)
  (let loop ((l '()) (n maxn))
    (cond ((= 0 n) l)
          (else (loop (cons n l) (- n 1))))))

(define (square n)
  (expt n 2))

(define (sumofsquares maxn)
  (apply + (map square (range maxn))))

(define (squareofsum maxn)
  (square (apply + (range maxn))))

(define (eul06 maxn)
  (abs (- (sumofsquares maxn) (squareofsum maxn))))

